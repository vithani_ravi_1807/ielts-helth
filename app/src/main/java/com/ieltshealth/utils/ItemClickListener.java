package com.ieltshealth.utils;

import android.view.View;

import com.ieltshealth.model.ExamTipsModel;

public interface ItemClickListener {
    void onItemClick(View view, int position, boolean isFromGameBooster, ExamTipsModel mExamTipsModel);
}