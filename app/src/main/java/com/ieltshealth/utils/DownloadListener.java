package com.ieltshealth.utils;

public interface DownloadListener {
    void onItemClick();
}