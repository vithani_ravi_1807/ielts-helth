package com.ieltshealth.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.StrictMode;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.ieltshealth.R;
import com.ieltshealth.activity.SubWritingScreen;
import com.ieltshealth.model.SubWritingCorrectionModel;
import com.ieltshealth.utils.Constants;
import com.ieltshealth.utils.InputStreamVolleyRequest;
import com.ieltshealth.utils.TLSSocketFactory;

import net.bohush.geometricprogressview.GeometricProgressView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class WritingSubAdapter extends RecyclerView.Adapter<WritingSubAdapter.MyViewHolder> {

    private List<SubWritingCorrectionModel> moviesList;
    Context context;
    boolean isFromGameBooster;
    MyViewHolder holder;
    String fileName;
    String exam;
    private static final int WRITE_REQUEST_CODE = 300;
    private static final String TAG = "";
    private String url;
    ProgressDialog progress;
    SubWritingScreen.UploadImage uploadImageListener;


    public WritingSubAdapter(List<SubWritingCorrectionModel> moviesList, Context context, String exam, SubWritingScreen.UploadImage uploadImageListener) {
        this.moviesList = moviesList;
        this.context = context;
        this.isFromGameBooster = isFromGameBooster;
        this.exam = exam;
        this.uploadImageListener = uploadImageListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.writing_sub_correction, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        this.holder = holder;
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        final SubWritingCorrectionModel mSubWritingCorrectionModel = moviesList.get(position);
        holder.tv_title.setText(mSubWritingCorrectionModel.getMain_title());
        holder.tv_preview.setText(mSubWritingCorrectionModel.getPreview());
        holder.commentText.setText(mSubWritingCorrectionModel.getComment());
        holder.tv_date.setText(mSubWritingCorrectionModel.getCreated_date());
        holder.gradText.setText(mSubWritingCorrectionModel.getGrade());
        holder.tv_description.setText(mSubWritingCorrectionModel.getDecription());
//        if(mSubWritingCorrectionModel.getFinal_time() != null && mSubWritingCorrectionModel.getFinal_time() != "null") {


        if (mSubWritingCorrectionModel.getFinal_time() != null && mSubWritingCorrectionModel.getFinal_time() != "null" && !mSubWritingCorrectionModel.getFinal_time().isEmpty()) {
            long futureTimestamp = Long.parseLong(mSubWritingCorrectionModel.getFinal_time());
            long timep = getDate(futureTimestamp);
            CountDownTimer cdt = new CountDownTimer(timep, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    /*long days = TimeUnit.MILLISECONDS.toDays(millisUntilFinished);
                    millisUntilFinished -= TimeUnit.DAYS.toMillis(days);*/

                    long hours = TimeUnit.MILLISECONDS.toHours(millisUntilFinished);
                    millisUntilFinished -= TimeUnit.HOURS.toMillis(hours);

                    long minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished);
                    millisUntilFinished -= TimeUnit.MINUTES.toMillis(minutes);

                    long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished);

                    holder.timerText.setText(hours + ":" + minutes + ":" + seconds); //You can compute the millisUntilFinished on hours/minutes/seconds
                }

                @Override
                public void onFinish() {
                    holder.timerText.setText("Finish!");
                }
            };
            cdt.start();
        }

        holder.btn_pdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Constants.isInternetAvailable(context)) {
                    downLoadPdf(mSubWritingCorrectionModel.getFile_name());
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });

        holder.btn_feedBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Constants.isInternetAvailable(context)) {
                    downLoadPdf(mSubWritingCorrectionModel.getFeedback_file());
//                    downLoadPdf("https://www.ieltsmedicalapp.com/android/img/file/190708031128.jpg");
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
        if (mSubWritingCorrectionModel.getStatus().equals("0")) {
            holder.btn_upload.setVisibility(View.GONE);
        } else if (mSubWritingCorrectionModel.getStatus().equals("1")) {
            holder.timerText.setVisibility(View.GONE);
            holder.gradText.setVisibility(View.VISIBLE);
            holder.commentText.setVisibility(View.VISIBLE);
            holder.btn_feedBack.setVisibility(View.VISIBLE);
            holder.btn_upload.setVisibility(View.GONE);
        } else {
            holder.timerText.setVisibility(View.VISIBLE);
            holder.gradText.setVisibility(View.GONE);
            holder.commentText.setVisibility(View.GONE);
            holder.btn_upload.setVisibility(View.VISIBLE);
            holder.btn_feedBack.setVisibility(View.GONE);
        }
        holder.btn_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Constants.isInternetAvailable(context)) {
                    uploadImageListener.onCLickUpload(mSubWritingCorrectionModel);
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    private long getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time * 1000);
        String date = DateFormat.format("dd-MM-yyyy hh:mm:ss", cal).toString();
        Date currentTime1 = cal.getTime();
        Date currentTime = Calendar.getInstance().getTime();

        long diff = currentTime1.getTime() - currentTime.getTime();
        long seconds = diff / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        return TimeUnit.SECONDS.toMillis(seconds);
    }


    private void downLoadPdf(String file_name) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US);
        Date now = new Date();
        fileName = formatter.format(now);
        url = file_name;
        downLoadFileUsingVolley(url);
    }




    void downLoadFileUsingVolley(final String mUrl) {
        final ProgressDialog mDialog = new ProgressDialog(context);
        mDialog.setMessage("Downloading File Please Wait...");
        mDialog.setCancelable(false);
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialog.show();

        InputStreamVolleyRequest request = new InputStreamVolleyRequest(Request.Method.GET, mUrl,
                new Response.Listener<byte[]>() {
                    @Override
                    public void onResponse(byte[] response) {
                        mDialog.dismiss();
                        // TODO handle the response
                        try {
                            File file;
                            FileOutputStream outputStream;
                            File directory = new File(Environment.getExternalStorageDirectory()+File.separator+"IELTS_Health");
                            if(!directory.exists()) {
                                directory.mkdirs();
                            }
                            String timestamp = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(new Date());
                            //Extract file name from URL
                            fileName = mUrl.substring(mUrl.lastIndexOf('/') + 1, mUrl.length());

                            //Append timestamp to file name
                            fileName = timestamp + "_" + fileName;
                            file = new File(directory.getAbsolutePath(), fileName);
                            outputStream = new FileOutputStream(file);
                            outputStream.write(response);
                            outputStream.close();

                            Toast.makeText(context, "File Download At "+directory , Toast.LENGTH_SHORT).show();



                        } catch (FileNotFoundException e1) {
                            mDialog.dismiss();
                            e1.printStackTrace();
                        } catch (IOException e1) {
                            mDialog.dismiss();
                            e1.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO handle the error
                error.printStackTrace();
                mDialog.dismiss();

            }
        }, null);
        RequestQueue mRequestQueue = Volley.newRequestQueue(context, new HurlStack());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            HttpStack stack = null;
            try {
                stack = new HurlStack(null, new TLSSocketFactory());
            } catch (KeyManagementException e) {
                e.printStackTrace();
                Log.d("Your Wrapper Class", "Could not create new stack for TLS v1.2");
                stack = new HurlStack();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                Log.d("Your Wrapper Class", "Could not create new stack for TLS v1.2");
                stack = new HurlStack();
            }
            mRequestQueue = Volley.newRequestQueue(context, stack);
        } else {
            mRequestQueue = Volley.newRequestQueue(context);
        }

        mRequestQueue.add(request);
    }




    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_title, tv_description, tv_date, tv_preview, gradText, commentText;
        CardView cv_test_click;
        Button btn_pdf, btn_upload, btn_feedBack;
        GeometricProgressView progressView;
        TextView timerText;

        public MyViewHolder(View view) {
            super(view);

            this.setIsRecyclable(false);

            progressView = view.findViewById(R.id.progressView);
            btn_pdf = view.findViewById(R.id.btn_pdf);
            btn_feedBack = view.findViewById(R.id.btn_feedBack);
            btn_upload = view.findViewById(R.id.btn_upload);
            cv_test_click = (CardView) view.findViewById(R.id.cv_test_click);
            tv_preview = (TextView) view.findViewById(R.id.tv_preview);
            commentText = (TextView) view.findViewById(R.id.commentText);
            gradText = (TextView) view.findViewById(R.id.gradText);
            tv_date = (TextView) view.findViewById(R.id.tv_date);
            tv_description = (TextView) view.findViewById(R.id.tv_description);
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            timerText = (TextView) view.findViewById(R.id.timerText);
        }
    }


}